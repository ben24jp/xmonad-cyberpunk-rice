import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import System.IO
import System.Exit (exitSuccess)

import qualified XMonad.StackSet as W
import qualified Data.Map        as M
import Data.Monoid
import XMonad.Actions.FloatSnap
import XMonad.Prompt
import XMonad.Prompt.Shell
import XMonad.Prompt.XMonad
import XMonad.Prompt.FuzzyMatch
import XMonad.Prompt.Man
import XMonad.Prompt.Ssh
import Data.Char (isSpace, toUpper)

import XMonad.Util.SpawnOnce
import XMonad.Util.Run(spawnPipe)
import XMonad.Layout.Spacing
import XMonad.Util.EZConfig
import XMonad.Actions.CycleWS
import XMonad.Hooks.ManageHelpers


myFont :: String
myFont = "xft:Hack Nerd Font:bold:size=9:antialias=true:hinting=true"

myTerminal      = "kitty"

myBorderWidth   = 1

myWorkspaces    = ["1","2","3","4","5","6","7","8","9"]

myManageHook = composeAll
    [ className =? "Gimp"      --> doFloat
    , className =? "Inkscape" --> doFloat
    , className =? "kitty" --> doCenterFloat
    ]

simsonXPConfig :: XPConfig
simsonXPConfig = def
      { font                = myFont
      , bgColor             = "#0c1012"
      , fgColor             = "#bbc2cf"
      , bgHLight            = "#3444cb"
      , fgHLight            = "#000000"
      , borderColor         = "#3444cb"
      , promptBorderWidth   = 1
      , position            = Bottom
      , height              = 25
      , historySize         = 256
      , historyFilter       = id
      , defaultText         = []
      , autoComplete        = Just 100000
      , showCompletionOnTab = False
      , searchPredicate     = fuzzyMatch
      , defaultPrompter     = id $ map toUpper
      , alwaysHighlight     = True
      , maxComplRows        = Nothing
      }

sshXPConfig :: XPConfig
sshXPConfig = def
      { font                = myFont
      , bgColor             = "#0c1012"
      , fgColor             = "#bbc2cf"
      , bgHLight            = "#3444cb"
      , fgHLight            = "#000000"
      , borderColor         = "#3444cb"
      , promptBorderWidth   = 1
      , position            = Bottom
      , height              = 25
      , historySize         = 256
      , historyFilter       = id
      , defaultText         = []
      , autoComplete        = Nothing
      , showCompletionOnTab = False
      , searchPredicate     = fuzzyMatch
      , defaultPrompter     = id $ map toUpper
      , alwaysHighlight     = True
      , maxComplRows        = Nothing
      }

myLayoutHook = spacingRaw True (Border 4 4 4 4) True (Border 4 4 4 4) True $ layoutHook defaultConfig

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    [ ((modm, xK_Return), spawn $ XMonad.terminal conf)
    , ((modm .|. shiftMask, xK_q), kill)
    , ((modm,               xK_space ), sendMessage NextLayout)
    , ((modm,               xK_n     ), refresh)
    , ((modm,               xK_j     ), windows W.focusDown)
    , ((modm,               xK_k     ), windows W.focusUp  )
    , ((modm,               xK_m     ), windows W.focusMaster  )
    , ((modm .|. shiftMask, xK_Return), windows W.swapMaster)
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )
    , ((modm,               xK_h     ), sendMessage Shrink)
    , ((modm,               xK_l     ), sendMessage Expand)
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))
    , ((modm .|. shiftMask, xK_period     ), io exitSuccess)
    , ((modm .|. shiftMask, xK_backslash), spawn "xmonad --recompile; xmonad --restart")
    , ((modm .|. shiftMask, xK_KP_Divide), spawn "xbacklight -set 0")
    , ((modm .|. shiftMask, xK_KP_Multiply), spawn "xbacklight -set 100")
    , ((modm .|. shiftMask, xK_KP_Subtract), spawn "xbacklight -dec 10")
    , ((modm .|. shiftMask, xK_KP_Add), spawn "xbacklight -inc 10")
    , ((modm, xK_a), spawn "rofi -modi drun -show drun -show-icons")
    , ((modm, xK_Print), spawn "screenshot.sh")
    , ((0, xK_Menu), shellPrompt simsonXPConfig)
    , ((modm .|. controlMask, xK_s), sshPrompt sshXPConfig)
    , ((modm, xK_F1), manPrompt simsonXPConfig)
    , ((modm,               xK_Left),  withFocused $ snapMove L Nothing)
    , ((modm,               xK_Right), withFocused $ snapMove R Nothing)
    , ((modm,               xK_Up),    withFocused $ snapMove U Nothing)
    , ((modm,               xK_Down),  withFocused $ snapMove D Nothing)
    , ((modm .|. shiftMask, xK_Left),  withFocused $ snapShrink R Nothing)
    , ((modm .|. shiftMask, xK_Right), withFocused $ snapGrow R Nothing)
    , ((modm .|. shiftMask, xK_Up),    withFocused $ snapShrink D Nothing)
    , ((modm .|. shiftMask, xK_Down),  withFocused $ snapGrow D Nothing)
    , ((modm, xK_i), incWindowSpacing 1)
    , ((modm, xK_d), decWindowSpacing 1)
    , ((modm .|. shiftMask, xK_i), incScreenSpacing 1)
    , ((modm .|. shiftMask, xK_d), decScreenSpacing 1)
    ]
    ++

        [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

        [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    [
         ((modm,               button1), (\w -> focus w >> mouseMoveWindow w >> afterDrag (snapMagicMove (Just 20) (Just 20) w)))
       , ((modm .|. shiftMask, button1), (\w -> focus w >> mouseMoveWindow w >> afterDrag (snapMagicResize [L,R,U,D] (Just 20) (Just 20) w)))
       , ((modm,               button3), (\w -> focus w >> mouseResizeWindow w >> afterDrag (snapMagicResize [R,D] (Just 20) (Just 20) w)))
       , ((modm,               button4), (\w -> nextWS))
       , ((modm,               button5), (\w -> prevWS))
    ]

mystartupHook = do
 spawnOnce "feh --bg-scale ~/.background/cp.png"
 spawnOnce "lxpolkit"
 spawnOnce "picom &"
 spawnOnce "xsetroot -cursor_name left_ptr &"

main = do
    xmproc <- spawnPipe "xmobar"
    xmonad $ docks defaultConfig
        { manageHook = myManageHook <+> manageHook defaultConfig
        , layoutHook = avoidStruts  $ myLayoutHook
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppCurrent = xmobarColor "#7070b8" "" . wrap "[" "]"
                        , ppVisible = xmobarColor "#7070b8" ""
                        , ppHidden = xmobarColor "#ad00ad" "" . wrap "*" ""
                        , ppHiddenNoWindows = xmobarColor "#3444cb" ""
                        , ppTitle = xmobarColor "#b3afc2" "" . shorten 50
                        }
        , modMask = mod4Mask
        , normalBorderColor  = "#7b8a83"
        , focusedBorderColor = "#3444cb"
        , startupHook        = mystartupHook
        , terminal           = myTerminal
        , keys               = myKeys
        , workspaces         = myWorkspaces
        , mouseBindings      = myMouseBindings
        , borderWidth        = myBorderWidth
        }
